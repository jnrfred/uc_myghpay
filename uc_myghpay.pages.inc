<?php

/**
 * GT Myghpay checkout menu items.
 * Finalizes myghpay check out transaction.
 */
 
function uc_myghpay_complete($cart_id = 0) {
	global $user;
	$base = base_path();
	$order_id = check_plain($_REQUEST['clientref']);
	$payment_status = check_plain($_REQUEST['statusCode']);
	//$trans_id = check_plain($_REQUEST['trans_id']);
	$order = uc_order_load($order_id);
	switch ($payment_status) {
		case 1: 
		//$comment = t('Paid by manilla order !order.', array('!order' => check_plain($_REQUEST['order_id'])));
		uc_payment_enter($order->order_id, 'myghpay', $order->order_total, $order->uid, 0, NULL);
		uc_cart_complete_sale($order);
		uc_order_comment_save($order->order_id, 0, t('Payment was made successfully via GT Bank myghpay.'));
		drupal_set_message('Your transaction was successful.');
		return t('Thank you for your transaction. Your transaction number is #'.$order->order_id. '. You may perform another transaction or <a href ='.$base.'user/@user_id/orders>view your current transaction status</a> and transaction history', array('@user_id'=>$user->uid));
		break;
		case 0:
		drupal_set_message(t('An error has occurred during payment'), 'error');
		drupal_goto('cancelled');
		break;
	}
}
